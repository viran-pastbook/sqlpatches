ALTER TABLE `orders` MODIFY COLUMN `currency` enum('USD','EUR','GBP','AUD','CAD','NZD','INR', 'MYR','PHP','SGD','THB') DEFAULT 'USD';


ALTER TABLE `order_transactions` MODIFY COLUMN `currency` enum('USD','EUR','GBP','AUD','CAD','NZD','INR', 'MYR','PHP','SGD','THB') DEFAULT 'USD';


ALTER TABLE `vouchers` MODIFY COLUMN `value_unit` enum('USD','EUR','GBP','AUD','CAD', 'NZD','INR', 'MYR','PHP','SGD','THB','%') DEFAULT '%';




